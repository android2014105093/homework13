package com.example.and.myapplication;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Implementation of App Widget functionality.
 */
public class MyAppWidgetProvider extends AppWidgetProvider {

    public static SensorManager sensorManager;
    public static HashMap<Integer,Integer> mCountMap = new HashMap<Integer, Integer>();
    private static HashMap<Integer,PendingIntent> mActionMap;

    static float currentY=0,previousY=0;
    static  int threshold = 10;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.my_app_widget_provider);
        views.setTextViewText(R.id.appwidget_text, widgetText);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
    private SensorEventListener stepDetector = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float y = event.values[1];
            currentY = y;
            if (Math.abs(currentY - previousY) > threshold) {
                //steps++;
                Iterator iterator = mCountMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    HashMap.Entry entry = (HashMap.Entry) iterator.next();
                    entry.setValue((Integer) entry.getValue() + 1);
                    Log.i("aaa", entry.getValue().toString());
                }
                // stepsTextView.setText(String.valueOf(steps));
            }
            previousY = y;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    @Override
    public void onReceive(Context context,Intent intent){
        super.onReceive(context, intent);
        sensorManager = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
        sensorManager.registerListener(stepDetector,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        Log.i("Android","onReceive");
    }
    public static void updateCount(String packageName,AppWidgetManager appWidgetManager, int appWidgetId){
        //float previousY,currentY;
        Integer steps = mCountMap.get(appWidgetId);
        if(steps==null)
            steps = 0;
        Log.i("111",String.valueOf(appWidgetId).toString());
        //Pedometer pedometer1 = new Pedometer();
        String output;
        output = "Count : "+steps;
//        steps++;
        mCountMap.put(appWidgetId,steps);
        RemoteViews views = new RemoteViews(packageName,
                R.layout.my_app_widget_provider);
        //views.setImageViewResource(R.id.imageView1,resid);
        views.setTextViewText(R.id.appwidget_text,output);
        appWidgetManager.updateAppWidget(appWidgetId,views);
    }
    public static PendingIntent makeUpdateAction(Context context, String command, int appWidgetId){
        Intent active = new Intent(context, WidgetService.class);
        active.setAction(command);
        active.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        Uri data = Uri.withAppendedPath(
                Uri.parse("myappwidgetservice://widget/id/#" + command + appWidgetId),
                String.valueOf(appWidgetId));
        active.setData(data);

        PendingIntent action = PendingIntent.getService(
                context, 0, active, PendingIntent.FLAG_UPDATE_CURRENT);
        return action;
    }
    public static int mUpdateInterval;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onUpdate");
        for(int appWidgetId : appWidgetIds){
            updateCount(context.getPackageName(),appWidgetManager,appWidgetId);
            PendingIntent action;
            action = mActionMap.get(appWidgetId);
            if (action == null) {
                action = makeUpdateAction(context, WidgetService.UPDATE, appWidgetId);
                mActionMap.put(appWidgetId, action);
                setAlarm(context, mUpdateInterval, action);
            }
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }
    public static void setAlarm(Context context,
                                int updateInterval, PendingIntent action) {
        AlarmManager alarmManager = (AlarmManager)
                context.getSystemService(Context.ALARM_SERVICE);
        if (updateInterval > 0) {alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime(), updateInterval, action);
        } else {
            alarmManager.cancel(action);
        }
    }
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onDeleted");
        for (int appWidgetId : appWidgetIds) {
            PendingIntent action = mActionMap.get(appWidgetId);
            if (action != null) {
                setAlarm(context, 0, action);
                mActionMap.remove(appWidgetId);
            }
        }
        super.onDeleted(context, appWidgetIds);
    }
    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onDisabled");
        context.stopService(new Intent(context,WidgetService.class));
        mActionMap.clear();
        mActionMap = null;
        super.onDisabled(context);
    }
    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        Log.i("AndroidRuntime", "MyAppWidgetProvider.onEnabled");
        mActionMap = new HashMap<Integer, PendingIntent>();
        mUpdateInterval = 1000;
        super.onEnabled(context);
    }

}

