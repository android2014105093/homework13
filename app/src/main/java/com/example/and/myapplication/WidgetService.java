package com.example.and.myapplication;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;

public class WidgetService extends Service {
    public static final String UPDATE =
            "Update MyappWidget";
    public WidgetService() {
    }
    @Override
    public void onStart(Intent intent,int startId){
        String command = intent.getAction();
        int appWidgetId =
                intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
        AppWidgetManager appWidgetManager =
                AppWidgetManager.getInstance(getApplicationContext());
        if(command.equals(UPDATE)){
            MyAppWidgetProvider.updateCount(
                    getApplicationContext().getPackageName(),
                    appWidgetManager,appWidgetId);
        }
        super.onStart(intent,startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
       // throw new UnsupportedOperationException("Not yet implemented");
    return null;
    }
}
